;
;-------Auxiliary facts ---------------------------------------
;

(defrule AGENT::initCycle-speeding
    (declare (salience 99))
    (timp (valoare ?)) ;make sure it fires each cycle
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "" crlf))
    ;(facts AGENT)
)

(defrule AGENT::init_speeding
     (declare (salience 99))
     (timp (valoare 1))
     ?f<-(speeding (speed ?v)(default ?d))
=>
     (assert (ag_bel (bel_type fluent) (bel_pname speeding-limit) (bel_pval ?v)))
     (assert (ag_bel (bel_type fluent) (bel_pname speeding-default) (bel_pval ?d)))
     (assert (ag_bel (bel_type fluent) (bel_pname road_type) (bel_pval national)))
     (assert (ag_bel (bel_type fluent) (bel_pname in_locality) (bel_pval false)))
     (if (eq ?*ag-in-debug* TRUE) then (printout t "    init speeed " crlf))
     (retract ?f)
)

;;---------------------------------------
;;
;;      Made by us
;;
;;---------------------------------------


;--- Sign forbidding a certain speeding
(defrule AGENT::sign_speed_limit
     (declare (salience 40))
     (timp (valoare ?t))
     ?f <- (ag_percept (percept_pobj road_sign) (percept_pname forbidden_over ) (percept_pval ?limita))
     ?s <- (ag_bel (bel_type fluent)(bel_pname speeding-limit) (bel_pval ?))
=>
     (modify ?s (bel_pval ?limita))
     (if (eq ?*ag-in-debug* TRUE) then (printout t " interzis peste ?speed_value "  crlf))
     (retract ?f)
)

; ;--- Sign ending certain speed limit
(defrule AGENT::sign_speed_limit_end
      (declare (salience 50))
      (timp (valoare ?t))
      ?f <- (ag_percept (percept_pobj road_sign) (percept_pname end_of_forbidden_over) (percept_pval ?limita))
      ?s <- (ag_bel (bel_type fluent) (bel_pname speeding-limit) (bel_pval ?current))
      ?d <- (ag_bel (bel_type fluent) (bel_pname speeding-default) (bel_pval ?old))
=>
      (if (eq ?current ?limita) then (modify ?s (bel_pval ?old)))
      (retract ?f)
)

;--- Sign entering localitate
(defrule AGENT::r-sign-entering-localitate
     (declare (salience 51))
     (timp (valoare ?t))
     ?f <- (ag_percept (percept_pobj road_sign) (percept_pname localitate) (percept_pval entering))
     ?s <- (ag_bel (bel_type fluent) (bel_pname speeding-limit)(bel_pval ?))
     ?d <- (ag_bel (bel_type fluent) (bel_pname speeding-default)(bel_pval ?))
     ?i <- (ag_bel (bel_type fluent) (bel_pname in_locality) (bel_pval ?))
=>
     (if (eq ?*ag-in-debug* TRUE) then (printout t "   am intrat in localitate si am viteza maxima : ?value "  crlf))
     (modify ?s (bel_pval 50))
     (modify ?d (bel_pval 50))
     (modify ?i (bel_pval true))
     (retract ?f)
)

;--- Sign we left localitate
(defrule AGENT::r-sign-leaving-localitate
     (declare (salience 45))
     (timp (valoare ?t))
     ?f <- (ag_percept (percept_pobj road_sign) (percept_pname localitate) (percept_pval leaving))
     ?g <- (ag_percept (percept_pobj gps_info) (percept_pname tip_drum) (percept_pval ?tip))
     ?s <- (ag_bel (bel_type fluent)(bel_pname speeding-limit) (bel_pval ?))
     ?d <- (ag_bel (bel_type fluent) (bel_pname speeding-default)(bel_pval ?))
     ?i <- (ag_bel (bel_type fluent) (bel_pname in_locality) (bel_pval ?))
     ?l <- (ag_bel (bel_type fluent) (bel_pname road_type) (bel_pval ?))
=>
     (if (eq ?*ag-in-debug* TRUE) then (printout t " am iesit din localitate" crlf))
     (if (eq ?tip judetean) then (modify ?s (bel_pval 90)) (modify ?d (bel_pval 90)) (modify ?l (bel_pval judetean)))
     (if (eq ?tip national) then (modify ?s (bel_pval 90)) (modify ?d (bel_pval 90)) (modify ?l (bel_pval national)))
     (if (eq ?tip european) then (modify ?s (bel_pval 100)) (modify ?d (bel_pval 100)) (modify ?l (bel_pval european)))
     (if (eq ?tip autostrada) then (modify ?s (bel_pval 130)) (modify ?d (bel_pval 130)) (modify ?l (bel_pval autostrada)))
     (modify ?i (bel_pval false))
     (retract ?f)
     (retract ?g)
)


; ;--- Sign entering on a national european road
(defrule AGENT::r-drum-european
     (declare (salience 50))
     (timp (valoare ?t))
     ?f <- (ag_percept (percept_pobj road_sign) (percept_pname drum_european) (percept_pval entering))
     ?s <- (ag_bel (bel_type fluent) (bel_pname speeding-limit)(bel_pval ?))
     ?d <- (ag_bel (bel_type fluent) (bel_pname speeding-default)(bel_pval ?))
     ?l <- (ag_bel (bel_type fluent) (bel_pname road_type) (bel_pval ?))
=>
     (modify ?s (bel_pval 100))
     (modify ?d (bel_pval 100))
     (modify ?l (bel_pval european))
     (if (eq ?*ag-in-debug* TRUE) then (printout t "   am intrat in drum_european"  crlf))
     (retract ?f)
)


;---Sign zona rezidentiala
(defrule AGENT::r-sign-zona-rezidentiala
     (declare (salience 50))
     (timp (valoare ?t))
     ?f <- (ag_percept (percept_pobj road_sign) (percept_pname zona_rezidentiala) (percept_pval ?val))
     ?s <- (ag_bel (bel_type fluent) (bel_pname speeding-limit)(bel_pval ?))
     ?d <- (ag_bel (bel_type fluent) (bel_pname speeding-default) (bel_pval ?def))
=>
     (if (eq ?val entering) then (modify ?s (bel_pval 20)))
     (if (eq ?val leaving)  then (modify ?s (bel_pval ?def)))
     (if (eq ?*ag-in-debug* TRUE) then (printout t "   am intrat in zona rezidentiala"  crlf))
     (retract ?f)
)

;---Sign end of zona rezidentiala
(defrule AGENT::r-sign-end-of-zona-rezidentiala
     (declare (salience 50))
     (timp (valoare ?t))
     ?f <- (ag_percept (percept_pobj road_sign) (percept_pname zona_rezidentiala) (percept_pval leaving))
     ?s <- (ag_bel (bel_type fluent) (bel_pname speeding-limit)(bel_pval ?))
     ?d <- (ag_bel (bel_type fluent) (bel_pname speeding-default) (bel_pval ?def))
=>
     (modify ?s (bel_pval ?def))
     (if (eq ?*ag-in-debug* TRUE) then (printout t "  am iesit din zona rezidentiala"  crlf))
     (retract ?f)
)

;---Sign zona pietonala
(defrule AGENT::r-sign-zona-pietonala
     (declare (salience 50))
     (timp (valoare ?t))
     ?f <- (ag_percept (percept_pobj road_sign) (percept_pname zona_pietonala) (percept_pval ?val))
     ?s <- (ag_bel (bel_type fluent) (bel_pname speeding-limit)(bel_pval ?))
     ?d <- (ag_bel (bel_type fluent) (bel_pname speeding-default) (bel_pval ?def))
=>
     (if (eq ?val entering) then (modify ?s (bel_pval 5)))
     (if (eq ?val leaving)  then (modify ?s (bel_pval ?def)))
     (if (eq ?*ag-in-debug* TRUE) then (printout t "   am intrat in zona pietonala"  crlf))
     (retract ?f)
)

;---Sign end of zona pietonala
(defrule AGENT::r-sign-end-of-zona-pietonala
     (declare (salience 50))
     (timp (valoare ?t))
     ?f <- (ag_percept (percept_pobj road_sign) (percept_pname zona_pietonala) (percept_pval leaving))
     ?s <- (ag_bel (bel_type fluent) (bel_pname speeding-limit)(bel_pval ?))
     ?d <- (ag_bel (bel_type fluent) (bel_pname speeding-default) (bel_pval ?def))
=>
     (modify ?s (bel_pval ?def))
     (if (eq ?*ag-in-debug* TRUE) then (printout t "   am iesit din zona pietonala"  crlf))
     (retract ?f)
)

;---Sign drum in lucru
(defrule AGENT::r-sign-drum-in-lucru-limitare-viteza
     (declare (salience 40))
     (timp (valoare ?t))
     ?f <- (ag_percept (percept_pobj road_sign) (percept_pname drum_in_lucru) (percept_pval ?v))
     ?s <- (ag_bel (bel_type fluent) (bel_pname speeding-limit)(bel_pval ?))
=>
     (modify ?s (bel_pval ?v))
     (if (eq ?*ag-in-debug* TRUE) then (printout t "   am intrat pe un drum in lucru"  crlf))
     (retract ?f)
)

;--Sign trecere de pietoni
(defrule AGENT::r-sign-trecere-de-pietoni
     (declare (salience 40))
     (timp (valoare ?t))
     ?b <- (ag_percept (percept_pobj gps_info) (percept_pname lanes) (percept_pval 1))
     ?p <- (ag_percept (percept_pobj pieton) (percept_pname distance_to_trecere) (percept_pval close))
     ?f <- (ag_percept (percept_pobj road_sign) (percept_pname information_sign) (percept_pval trecere_de_pietoni))
     ?s <- (ag_bel (bel_type fluent) (bel_pname speeding-limit)(bel_pval ?))
=>
     (modify ?s (bel_pval 30))
     (if (eq ?*ag-in-debug* TRUE) then (printout t "   am intrat in localitate"  crlf))
     (retract ?b)
     (retract ?p)
     (retract ?f)
)

;---Sign curba deosebit de periculoasa
(defrule AGENT::r-sign-curba-deosebit-de-periculoasa
     (declare (salience 40))
     (timp (valoare ?t))
     ?f <- (ag_percept (percept_pobj road_sign) (percept_pname warning_sign) (percept_pval curba_deosebit_de_periculoasa))
     ?s <- (ag_bel (bel_type fluent) (bel_pname speeding-limit)(bel_pval ?))
     ?i <- (ag_bel (bel_type fluent) (bel_pname in_locality) (bel_pval ?boolean))
=>
     (if (eq ?boolean true) then (modify ?s (bel_pval 30)))
     (if (eq ?boolean false) then (modify ?s (bel_pval 50)))
     (if (eq ?*ag-in-debug* TRUE) then (printout t "   am intrat in localitate"  crlf))
     (retract ?f)
)

;---Sign entering highway
(defrule AGENT::r-sign-autostrada
     (declare (salience 40))
     (timp (valoare ?t))
     ?f <- (ag_percept (percept_pobj road_sign) (percept_pname autostrada) (percept_pval entering))
     ?c <- (ag_percept (percept_pobj gps_info) (percept_pname country) (percept_pval ?country))
     ?s <- (ag_bel (bel_type fluent) (bel_pname speeding-limit)(bel_pval ?))
     ?d <- (ag_bel (bel_type fluent) (bel_pname speeding-default) (bel_pval ?))
     ?l <- (ag_bel (bel_type fluent) (bel_pname road_type) (bel_pval ?))
=>
     (if (eq ?country Germania) then (modify ?s (bel_pval none)) (modify ?d (bel_pval none))
     else (modify ?s (bel_pval 130)) (modify ?d (bel_pval 130)))
     (if (eq ?*ag-in-debug* TRUE) then (printout t "   am intrat in localitate"  crlf))
     (modify ?l (bel_pval autostrada))
     (retract ?f)
     (retract ?c)
)

;---Sign exiting highway
(defrule AGENT::sign-autostrada-exit
     (declare (salience 50))
     (timp (valoare ?t))
     ?f <- (ag_percept (percept_pobj road_sign) (percept_pval autostrada_exit))
     ?s <- (ag_bel (bel_type fluent) (bel_pname speeding-limit)(bel_pval ?))
     ?d <- (ag_bel (bel_type fluent) (bel_pname speeding-default) (bel_pval ?))
     ?l <- (ag_bel (bel_type fluent) (bel_pname road_type) (bel_pval ?))
=>
     (modify ?s (bel_pval) 90)
     (modify ?d (bel_pval) 90)
     (if (eq ?*ag-in-debug* TRUE) then (printout t "   am intrat in localitate"  crlf))
     (modify ?l (bel_pval /NA))
     (retract ?f)
)

;--Senzorul de ploaie ne atentioneaza ca incepe sa ploua
(defrule AGENT::r-raining
     (declare (salience 40))
     (timp (valoare ?t))
     ?p <- (ag_percept (percept_pobj raining_sensor) (percept_pname is_raining) (percept_pval true))
     ?s <- (ag_bel (bel_type fluent) (bel_pname speeding-limit)(bel_pval ?))
     ?l <- (ag_bel (bel_type fluent) (bel_pname road_type) (bel_pval ?road))
     ?i <- (ag_bel (bel_type fluent) (bel_pname in_locality) (bel_pval ?boolean))
=>
     (if (eq ?road autostrada) then (modify ?s (bel_pval 80)) else
       (if (eq ?boolean true) then (modify ?s (bel_pval 30)))
       (if (eq ?boolean false) then (modify ?s (bel_pval 50)))
     )
     (if (eq ?*ag-in-debug* TRUE) then (printout t "   am intrat in localitate"  crlf))
     (retract ?p)
)

;--Senzorul de ploaie ne atentioneaza ca nu mai ploua
(defrule AGENT::r-rain-stops
     (declare (salience 40))
     (timp (valoare ?t))
     ?p <- (ag_percept (percept_pobj raining_sensor) (percept_pname is_raining) (percept_pval false))
     ?s <- (ag_bel (bel_type fluent) (bel_pname speeding-limit)(bel_pval ?))
     ?d <- (ag_bel (bel_type fluent) (bel_pname speeding-default) (bel_pval ?default))
=>
     (modify ?s (bel_pval ?default))
     (if (eq ?*ag-in-debug* TRUE) then (printout t "   am intrat in localitate"  crlf))
     (retract ?p)
)

;--Senzorul masinii ne anunta ca am trecut de trecerea de pietoni
(defrule AGENT::r-trecere-de-pietoni-end
     (declare (salience 40))
     (timp (valoare ?t))
     ?p <- (ag_percept (percept_pobj car_sensor) (percept_pname trecere_de_pietoni) (percept_pval ended))
     ?s <- (ag_bel (bel_type fluent) (bel_pname speeding-limit)(bel_pval ?))
     ?d <- (ag_bel (bel_type fluent) (bel_pname speeding-default) (bel_pval ?default))
=>
     (modify ?s (bel_pval ?default))
     (if (eq ?*ag-in-debug* TRUE) then (printout t "   am trecut de trecerea de pietoni"  crlf))
     (retract ?p)
)

;--Senzorul masinii ne anunta cand am iesit din curba periculoasa
(defrule AGENT::r-curba-deosebit-de-periculoasa-end
     (declare (salience 40))
     (timp (valoare ?t))
     ?p <- (ag_percept (percept_pobj car_sensor) (percept_pname curba_deosebit_de_periculoasa) (percept_pval ended))
     ?s <- (ag_bel (bel_type fluent) (bel_pname speeding-limit)(bel_pval ?))
     ?d <- (ag_bel (bel_type fluent) (bel_pname speeding-default) (bel_pval ?default))
=>
     (modify ?s (bel_pval ?default))
     (if (eq ?*ag-in-debug* TRUE) then (printout t "   am trecut de curba deosebit de periculoasa"  crlf))
     (retract ?p)
)

;--Semn de trecere de frontiera
(defrule AGENT::r-trecere-de-frontiera
     (declare (salience 40))
     (timp (valoare ?t))
     ?f <- (ag_percept (percept_pobj road_sign) (percept_pname information_sign) (percept_pval punct_vamal))
     ?g <- (ag_percept (percept_pobj road_sign) (percept_pname country) (percept_pval ?country))
     ?s <- (ag_bel (bel_type fluent) (bel_pname speeding-limit)(bel_pval ?))
     ?d <- (ag_bel (bel_type fluent) (bel_pname speeding-default) (bel_pval ?))
     ?l <- (ag_bel (bel_type fluent) (bel_pname road_type) (bel_pval autostrada))
=>
     (if (eq ?country Germania) then (modify ?s (bel_pval none)) (modify ?d (bel_pval none))
     else (modify ?s (bel_pval 130)) (modify ?d (bel_pval 130))
     )
     (if (eq ?*ag-in-debug* TRUE) then (printout t "   am trecut de curba deosebit de periculoasa"  crlf))
     (retract ?f)
     (retract ?g)
)



;--- End of all restrictions
(defrule AGENT::r-incetare-tuturor-restrictiilor
     (declare (salience 50))
     (timp (valoare ?t))
     ?f <- (ag_percept (percept_pobj road_sign) (percept_pname restrictions) (percept_pval ended))
     ?s <- (ag_bel (bel_type fluent)(bel_pname speeding-limit) (bel_pval ?))
     ?d <- (ag_bel (bel_type fluent) (bel_pname speeding-default)(bel_pval ?def))
=>
     (if (eq ?*ag-in-debug* TRUE) then (printout t " toate restrictiile intrerupte" crlf))
     (modify ?s (bel_pval ?def))
     (retract ?f)
)

;;---------------------------------------
;;
;;      End made by us
;;
;;---------------------------------------

;
;--------Print decision-----------------------------------
;
(defrule AGENT::tell
    (declare (salience -50))
    (timp (valoare ?)) ;make sure it fires each cycle
    (ASK ?bprop)
    ?fcvd <- (ag_bel (bel_type fluent) (bel_pname ?bprop) (bel_pval ?bval))
=>
    (printout t "         AGENT " ?bprop " " ?bval crlf)
    ;(retract ?fcvd)
)


;
;---------Housekeeping----------------------------------
;

;---------Delete auxiliary facts----------

;---------Delete instantaneous beliefs, i.e, those which are not fluents
(defrule AGENT::hk-eliminate-momentan-current-bel
    (declare (salience -90))
    (timp (valoare ?)) ;make sure it fires each cycle
    ?fmcb <- (ag_bel (bel_type moment) (bel_timeslice 0) (bel_pname ?p) (bel_pval ?v))
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>hk-eliminate-momentan-current-bel " ?p " " ?v crlf))
    (retract ?fmcb)
)
